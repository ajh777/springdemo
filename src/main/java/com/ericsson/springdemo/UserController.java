package com.ericsson.springdemo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/user")
public class UserController {
	private UserRepository userRepo;

	/*
	 * Param @userRepo will be instantiated by Spring Framework
	 * */
	public UserController(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@GetMapping("/list")
	public String userList(Model model) {
		model.addAttribute("users", userRepo.findAll());

		return "index"; // View's name
	}

	@GetMapping("/list-json-1")
	@ResponseBody // Write directly to the body of response instead of rendering with a view
	public Iterable<User> userListJson1(Model model) {
		return userRepo.findAll();
	}

	@GetMapping("/list-json-2")
	public ResponseEntity<Iterable<User>> userListJson2(Model model) {
		return new ResponseEntity<>(userRepo.findAll(), HttpStatus.OK);
	}

}
