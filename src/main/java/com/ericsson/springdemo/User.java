package com.ericsson.springdemo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // Mark as JPA entity
public class User {
	@Id // Designate it as the property that will uniquely identify the entity in the database
	@GeneratedValue(strategy=GenerationType.AUTO) // Rely on the DB to automatically generate the ID value
	private Long id;

	private String name;
	private String role;

	public User() {} // Default constructor is required by JPA

	public User(String name, String role) {
		super();
		this.name = name;
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
