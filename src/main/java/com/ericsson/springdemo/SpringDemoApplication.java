package com.ericsson.springdemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoApplication.class, args);
	}

	/*
	 * Param @userRepo will be instantiated properly by Spring Framework
	 * */
	@Bean
	public CommandLineRunner dataLoader(UserRepository userRepo) {
		return new CommandLineRunner() {
			@Override
			public void run(String... args) throws Exception {
				userRepo.save(new User("Andy", "User"));
				userRepo.save(new User("Bill", "User"));
				userRepo.save(new User("Cindy", "Admin"));
			}
			
		};
	}
}

