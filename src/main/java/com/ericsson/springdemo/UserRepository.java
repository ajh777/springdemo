package com.ericsson.springdemo;

import org.springframework.data.repository.CrudRepository;

// Spring Data JPA automatically generates an implementation on the fly.
public interface UserRepository extends CrudRepository<User, Long> {

}
