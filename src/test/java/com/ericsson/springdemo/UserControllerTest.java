package com.ericsson.springdemo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class UserControllerTest {
	@Autowired
	private TestRestTemplate rest;

	@Test
	public void testUserList() throws Exception {
		ResponseEntity<String> rsp = rest.getForEntity("/user/list-json-1", String.class);

		assertThat(rsp.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(rsp.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);

		JSONArray jsonArray = new JSONArray(rsp.getBody());
		assertEquals(jsonArray.length(), 3);

		JSONObject obj0 = (JSONObject)jsonArray.get(0);
		JSONObject obj1 = (JSONObject)jsonArray.get(1);
		JSONObject obj2 = (JSONObject)jsonArray.get(2);

		assertEquals(obj0.get("name"), "Andy");
		assertEquals(obj0.get("role"), "User");
		assertEquals(obj1.get("name"), "Bill");
		assertEquals(obj1.get("role"), "User");
		assertEquals(obj2.get("name"), "Cindy");
		assertEquals(obj2.get("role"), "Admin");
	}

	@Test
	public void testUserList2() throws Exception {
		ResponseEntity<User[]> rsp = rest.getForEntity("/user/list-json-1", User[].class);

		assertThat(rsp.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(rsp.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);

		User[] users = rsp.getBody();
		assertEquals(users.length, 3);

		assertEquals(users[0].getName(), "Andy");
		assertEquals(users[0].getRole(), "User");
		assertEquals(users[1].getName(), "Bill");
		assertEquals(users[1].getRole(), "User");
		assertEquals(users[2].getName(), "Cindy");
		assertEquals(users[2].getRole(), "Admin");
	}
}
